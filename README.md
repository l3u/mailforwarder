# mailforwarder

This project has been archived and is no longer developed. Simply use something more advanced somebody else wrote for this exact task, like [msmtp](https://marlam.de/msmtp/).

## Simple SMTP server able to forward mails from the LAN to the WAN

The purpose of this server is to forward emails from a (closed) LAN to the Internet. If machines inside a LAN don't have internet access, but can talk to a machine that has, this one can run this server to forward outgoing emails from inside the LAN.

It's intended to be used with implicit SSL/TLS (normally via port 465), not STARTTLS or unencrypted connections (`smtplib`'s `SMTP_SSL` class is used for the communication with the server).

## Installation

Simply put this script somewhere you can execute it (maybe in `/usr/local/sbin` or such) and run it. As it's intended to be run as a daemon, you may want to add some init script. On [Gentoo's](https://gentoo.org/) [OpenRC](https://wiki.gentoo.org/wiki/Project:OpenRC), this would e. g. look like this:

    depend() {
            need net
    }

    start() {
            ebegin "Starting mailforwarder"
            start-stop-daemon --background --start -u mail \
                    --exec /usr/local/bin/mailforwarder \
                    --make-pidfile --pidfile /var/run/mailforwarder.pid
            eend $?
    }

    stop() {
            ebegin "Stopping mailforwarder"
            start-stop-daemon --stop \
                    --exec /usr/local/bin/mailforwarder \
                    --pidfile /var/run/mailforwarder.pid
            eend $?
    }

## Configuration

The server reads the config file `/etc/mailforwarder` on startup. It has the following content:

    [setup]
    local_ip=<your local IP address>
    local_port=<port the server should listen to>
    server=<SMTP server to use>
    remote_port=<the SMTP server's port>
    user=<user name to log in>
    pass=<password to log in>
    bcc=<email address for a BCC>

Clients in the closed LAN can then connect to the IP and port given in `local_ip` and `local_port` (without any authentication) to send emails and they will be forwarded to the given server.

That's it :-)
